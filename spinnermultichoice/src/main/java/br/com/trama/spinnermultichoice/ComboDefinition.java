package br.com.trama.spinnermultichoice;

import java.util.ArrayList;
import java.util.List;


abstract class ComboDefinition {

	public List<String> getSelectedValues(boolean[] selected, String[] itens){
		
		List<String> selectedValues = new ArrayList<String>();
		
		for(int i = 0; i < selected.length; i++){

			if(selected[i]){
				selectedValues.add(itens[i]);
			}
		}
		
		return selectedValues;
	}
	
	public String valueShow(boolean[] selected, String[] itens, String defaultComboValue){

		boolean isSelected = false;

		StringBuilder builder = new StringBuilder("");

		for(int i = 0; i < selected.length; i++){

			if(selected[i]){
				
				if(isSelected){
					builder.append(", ");
				}

				builder.append(itens[i]);
				
				isSelected = true;
			}
		}

		//se nao selecinou nenhuma adiciona o texto padrao
		if(!isSelected){
			builder.append(defaultComboValue);
		}

		String string = builder.toString();

/*		int lastIndex = string.lastIndexOf(",");

		//remove a ultima virgula caso exista
		if(lastIndex > -1){
			string = string.substring(0, lastIndex);
		}*/

		return string;
	}
}
