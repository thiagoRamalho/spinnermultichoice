package br.com.trama.spinnermultichoice;

import java.util.Collections;
import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.ListView;

public class AllSelectionStrategy extends ComboDefinition implements SelectionStrategy{
	
	public void execute(DialogInterface dialog, int which, boolean isChecked, boolean selected[]) {

		//se selecionou a opcao todos...
		if(which == 0){

			allSelected(dialog, isChecked, selected);
		}
		//se desmarcou alguma opcao e o todos esta mercado...
		else if(!isChecked && selected[0]){

			allDeselected(dialog, selected);
		} 
		//demais casos
		else {
			selected[which] = isChecked;
		}
	}

	private void allDeselected(DialogInterface dialog, boolean[] selected) {
		ListView listView = getListView(dialog);
		listView.setItemChecked(0, false);
		selected[0] = false;
	}

	private void allSelected(DialogInterface dialog, boolean isChecked, boolean[] selected) {
		
		for(int i = 0 ; i < selected.length; i++){
			ListView listView = getListView(dialog);

			listView.setItemChecked(i, isChecked);
			selected[i] = isChecked;
		}
	}

	private ListView getListView(DialogInterface dialog) {
		AlertDialog alert = (AlertDialog)dialog;
		ListView listView = alert.getListView();
		
		return listView;
	}

	public String defineComboValue(boolean[] selected, String[] itens, String defaultComboValue) {
		
		String valueShow = valueShow(selected, itens, defaultComboValue);

		List<String> selectedValues = getSelectedValues(selected, itens);
		
		if(selected.length > 0 && selectedValues.contains(itens[0])){
			valueShow = itens[0];
		}
		
		return valueShow;
	}
	
	public List<String> getSelectedValues(boolean[] selected, String[] itens) {
		
		//se selecionou a primeira opcao devolve a mesma apenas
		if(selected[0]){
			return Collections.singletonList(itens[0]);
		}
		
		//do contrario filtra todas elas
		return super.getSelectedValues(selected, itens);
	}
}
