package br.com.trama.spinnermultichoice;

import android.app.Activity;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.widget.EditText;

public class HelloAndroidActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        MultiChoiceSpinner multiSpinner = (MultiChoiceSpinner) findViewById(R.id.multiChoiceSpinner1);
        multiSpinner.setSelectionStrategy(new AllSelectionStrategy());
        multiSpinner.setItens(new String[]{"All", "Select 1","Select 2","Select 3","Select 4","Select 5"}, "Select One");
        
        EditText phone = (EditText) findViewById(R.id.phoneText1);
        phone.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
    }
}

