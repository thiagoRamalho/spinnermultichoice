package br.com.trama.spinnermultichoice;

import java.util.List;

import android.R.attr;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.util.AttributeSet;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class MultiChoiceSpinner extends Spinner implements OnMultiChoiceClickListener{

	private static final String OK = "Ok";
	private static final String SELECT_ONE_OPTION = "Options";
	private boolean[] selected = new boolean[0];
	private String[]  itens = new String[0];
	private String defaultValueSelected;
	private SelectionStrategy selectionStrategy;

	public MultiChoiceSpinner(Context context) {
		this(context, null);
	}

	public MultiChoiceSpinner(Context context, AttributeSet attr) {
		super(context, attr);
		this.setSelectionStrategy(new DefaultSelectionStrategy());
		this.setValuesSelectedSpinner();
	}

	public void onClick(DialogInterface dialog, int which, boolean isChecked) {
		selectionStrategy.execute(dialog, which, isChecked, selected);
	}

	/**
	 * A Estrategia define a opcao onde o primeiro elemento da lista sera considerado
	 * todos
	 * 
	 * @param selectionStrategy
	 */
	public void setSelectionStrategy(SelectionStrategy selectionStrategy) {
		this.selectionStrategy = selectionStrategy;
	}
	
	/**
	 * Cria o dialog que sera exibido com a listagem enviada
	 * 
	 */
	@Override
	public boolean performClick() {

		AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

		builder.setMultiChoiceItems(itens, selected, this);
		builder.setTitle(SELECT_ONE_OPTION);
		
        builder.setPositiveButton(OK, new ClickListener());
        builder.setOnCancelListener(null);
        builder.show();

		return true;
	}

	/**
	 * @param itens - Itens que serao exibidos para multi-selecao
	 * @param defaultValueSelected - valor que sera exibido no combo e titulo do dialog
	 * 
	 * Obs.: O valor exibido no combo so acontence ao iniciar o component ou caso nenhuma
	 *       opcao tenha sido selecionada
	 */
	public void setItens(String[] itens, String defaultValueSelected){
		this.itens = itens;
		this.defaultValueSelected = defaultValueSelected;
		this.selected = new boolean[this.itens.length];
		this.setValuesSelectedSpinner();
		
	}
	
	private void setValuesSelectedSpinner() {

		String values = extract();

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
				android.R.layout.simple_spinner_item, new String[] {values});

		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		this.setAdapter(adapter);
	}

	private String extract() {
		return this.selectionStrategy.defineComboValue(selected, itens, this.defaultValueSelected);
	}

	public List<String> getselectedItens(){
		return this.selectionStrategy.getSelectedValues(selected, itens);
	}

	@Override
	public Object getSelectedItem() {
		throw new IllegalAccessError("use getSelectedItens");
	}

	class ClickListener implements DialogInterface.OnClickListener {

		public void onClick(DialogInterface dialog, int which) {

			setValuesSelectedSpinner();
		}
	}
}


