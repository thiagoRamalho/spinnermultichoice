package br.com.trama.spinnermultichoice;

import java.util.List;

import android.content.DialogInterface;

public interface SelectionStrategy{

	void execute(DialogInterface dialog, int which, boolean isChecked, boolean selected[]);
	
	String defineComboValue(boolean[] selected, String[] itens, String defaultComboValue);
	
	List<String> getSelectedValues(boolean[] selected, String[] itens);
}
