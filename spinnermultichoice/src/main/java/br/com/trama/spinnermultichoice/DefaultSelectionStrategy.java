package br.com.trama.spinnermultichoice;

import android.content.DialogInterface;

class DefaultSelectionStrategy extends ComboDefinition implements SelectionStrategy{

	public void execute(DialogInterface dialog, int which, boolean isChecked, boolean[] selected) {
		selected[which] = isChecked;
	}

	public String defineComboValue(boolean[] selected, String[] itens, String defaultComboValue) {
		return valueShow(selected, itens, defaultComboValue);
	}
}